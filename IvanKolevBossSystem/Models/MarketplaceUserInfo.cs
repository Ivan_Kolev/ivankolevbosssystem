﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IvanKolevBossSystem.Models
{
    public class MarketplaceUserInfo
    {
        public MarketplaceUserInfo(ApplicationUser user, Sock sock)
        {
            this.User = user;
            this.Sock = sock;
        }
        public ApplicationUser User { get; set; }
        public Sock Sock { get; set; }
    }
}