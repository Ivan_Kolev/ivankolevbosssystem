﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace IvanKolevBossSystem.Models
{
    public class Sock
    {
        public Sock()
        {
            this.Date = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public decimal Price { get; set; }

        public DateTime Date { get; set; }

    }
}