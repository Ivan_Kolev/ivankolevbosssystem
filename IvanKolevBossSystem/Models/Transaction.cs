﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IvanKolevBossSystem.Models
{
    public class Transaction
    {
        public Transaction()
        {
            this.Date = DateTime.Now;   
        }
        [Key]
        public int Id { get; set; }

        [Required]
        public ApplicationUser User { get; set; }

        public int SockAmount { get; set; }

        public decimal CurrentSockPrice { get; set; }

        [Required]
        public decimal AmountOfMoney { get; set; }

        [Required]
        public string TypeOfTransaction { get; set; }

        [Required]
        public string CurrentAccBalance { get; set; }

        public DateTime Date { get; set; }
    }
}