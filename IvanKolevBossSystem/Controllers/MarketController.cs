﻿using IvanKolevBossSystem.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;


namespace IvanKolevBossSystem.Controllers
{
    public class MarketController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private Transaction transaction = new Transaction();


        public Sock getCurrentSockInfo()
        {
            Sock currentSockInfo = db.Socks.OrderByDescending(s => s.Id).FirstOrDefault();
            return currentSockInfo;
        }
        // GET: Market/Marketplace/Id
        public ActionResult Marketplace(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }


            var MarketplaceUserInfo = new MarketplaceUserInfo(applicationUser, getCurrentSockInfo());

            return View(MarketplaceUserInfo);
        }

        // POST: Market/Marketplace/Id
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Marketplace(string id, string money,string typeOfTransaction, string numberOfSocks = "")
        {
            var types = new string[] { "S", "D", "B" }; //allowed types

            ApplicationUser applicationUser = db.Users.Find(id);

            if (id == null || Array.IndexOf(types, typeOfTransaction) < 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var MarketplaceUserInfo = new MarketplaceUserInfo(applicationUser, getCurrentSockInfo());

            if (typeOfTransaction == "D")
            {
                //deposit
               
                decimal moneyToAdd;
                if (!decimal.TryParse(money, out moneyToAdd))
                {
                    ModelState.AddModelError("money", "Deposit amount must be a number!");
                    return View(MarketplaceUserInfo);
                }
                applicationUser.AvailableBalance += moneyToAdd;

                //transaction fill data
                transaction.AmountOfMoney = moneyToAdd;
                transaction.TypeOfTransaction = typeOfTransaction;
                transaction.User = applicationUser;
                transaction.CurrentAccBalance = applicationUser.AvailableBalance.ToString();


                //Success message : You have successfully deposited {moneyToAdd} to your account!

            }
            else if (typeOfTransaction == "B")
            {
                //buy                

                int NumberOfSocks;
                if (!int.TryParse(numberOfSocks, out NumberOfSocks))
                {
                    ModelState.AddModelError("numberOfSocks", "Number of socks must be a number!");
                    return View(MarketplaceUserInfo);
                }
                Sock sockInfo = getCurrentSockInfo();
                decimal moneyToWithdraw = NumberOfSocks * sockInfo.Price;
                if(moneyToWithdraw > applicationUser.AvailableBalance)
                {
                    ModelState.AddModelError("numberOfSocks", $"You don't have enough money to buy {NumberOfSocks} socks!");
                    return View(MarketplaceUserInfo);
                }
                applicationUser.AvailableBalance -= moneyToWithdraw;
                applicationUser.SocksCount += NumberOfSocks;
                //transaction fill data
                transaction.AmountOfMoney = moneyToWithdraw;
                transaction.TypeOfTransaction = typeOfTransaction;
                transaction.User = applicationUser;
                transaction.CurrentAccBalance = applicationUser.AvailableBalance.ToString();
                transaction.CurrentSockPrice = sockInfo.Price;
                transaction.SockAmount = NumberOfSocks;

                //Success message : You have successfully bought {NumberOfSocks} socks!

            }
            else 
            {
                //sell

                int NumberOfSocks;
                if (!int.TryParse(numberOfSocks, out NumberOfSocks))
                {
                    ModelState.AddModelError("numberOfSocks", "Number of socks must be a number!");
                    return View(MarketplaceUserInfo);
                }
                Sock sockInfo = getCurrentSockInfo();
                decimal moneyToAdd = NumberOfSocks * sockInfo.Price;
                if (NumberOfSocks > applicationUser.SocksCount)
                {
                    ModelState.AddModelError("numberOfSocks", $"You don't have enough socks to sell - {applicationUser.SocksCount} socks left!");
                    return View(MarketplaceUserInfo);
                }
                applicationUser.AvailableBalance += moneyToAdd;
                applicationUser.SocksCount -= NumberOfSocks;

                //transaction fill data
                transaction.AmountOfMoney = moneyToAdd;
                transaction.TypeOfTransaction = typeOfTransaction;
                transaction.User = applicationUser;
                transaction.CurrentAccBalance = applicationUser.AvailableBalance.ToString();
                transaction.CurrentSockPrice = sockInfo.Price;
                transaction.SockAmount = NumberOfSocks;

                //Success message : You have successfully sold {NumberOfSocks} socks!
            }

            if (ModelState.IsValid)
            {
                db.Transactions.Add(transaction);
                db.Entry(applicationUser).State = EntityState.Modified;
                db.SaveChanges();
            }

            return View(MarketplaceUserInfo);
        }

        // GET: Market/Transactions
        public ActionResult Transactions()
        {
            var transactions = db.Transactions.Where(t => t.TypeOfTransaction != "D").Include(t => t.User).OrderByDescending(t => t.Date).ToList();
            return View(transactions);
        }


        // GET: Market/Transactions/Id
        public ActionResult UserTransactions(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userTransactions = db.Transactions.Where(t => t.User.Id == applicationUser.Id).Include(t => t.User).OrderByDescending(t => t.Date).ToList();
            return View(userTransactions);
        }

    }
}