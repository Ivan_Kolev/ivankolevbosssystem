﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IvanKolevBossSystem.SockPriceUpdater;

namespace IvanKolevBossSystem.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Singleton.Instance.StartPriceUpdating();
            return View();
        }
    }
}