﻿using IvanKolevBossSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IvanKolevBossSystem.Controllers
{
    public class AjaxController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        private Transaction transaction = new Transaction();


        public Sock getCurrentSockInfo()
        {
            Sock currentSockInfo = db.Socks.OrderByDescending(s => s.Id).FirstOrDefault();
            return currentSockInfo;
        }

        // POST: Ajax/Deposit
        [HttpPost]
        public ActionResult Deposit(string userId, string money, string typeOfTransaction)
        {
            var types = new string[] { "S", "D", "B" }; //allowed types

            ApplicationUser applicationUser = db.Users.Find(userId);

            if (userId == null || Array.IndexOf(types, typeOfTransaction) < 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var MarketplaceUserInfo = new MarketplaceUserInfo(applicationUser, getCurrentSockInfo());

            if (typeOfTransaction == "D")
            {
                //deposit

                decimal moneyToAdd;
                if (!decimal.TryParse(money, out moneyToAdd))
                {
                    ModelState.AddModelError("money", "Deposit amount must be a number!");
                    return View(MarketplaceUserInfo);
                }
                applicationUser.AvailableBalance += moneyToAdd;

                //transaction fill data
                transaction.AmountOfMoney = moneyToAdd;
                transaction.TypeOfTransaction = typeOfTransaction;
                transaction.User = applicationUser;
                transaction.CurrentAccBalance = applicationUser.AvailableBalance.ToString();

                db.Transactions.Add(transaction);
                db.Entry(applicationUser).State = EntityState.Modified;
                db.SaveChanges();
                string msg = $"You've successfully deposited {transaction.AmountOfMoney} to your account";
                var result = new string[] { msg };
                return Json(msg);
            }
            string msgOuter = $"You've successfully deposited {transaction.AmountOfMoney} to your account";
            var resultOuter = new string[] { msgOuter };
            return Json(resultOuter);
        }
    }
}