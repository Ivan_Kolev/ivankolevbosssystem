﻿using IvanKolevBossSystem.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Timers;

namespace IvanKolevBossSystem.SockPriceUpdater
{
    public static class SockPriceUpdater
    {

        private static readonly Random random = new Random();

        private static readonly Timer aTimer;

        static SockPriceUpdater()
        {
            double interval = 60*60000.0; //1 hour
            aTimer = new Timer(interval);

            // Hook up the event handler for the Elapsed event.
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

            aTimer.Elapsed += new ElapsedEventHandler(UpdateSockPrice);

        }

        private static void UpdateSockPrice(object source, ElapsedEventArgs e)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Sock currentSockInfo = db.Socks.OrderByDescending(s => s.Id).FirstOrDefault();
            decimal newSockPrice = (decimal)RandomNumberBetween(0.5, 2) * currentSockInfo.Price;
            Sock sock = new Sock()
            {
                Price = newSockPrice
            };
            db.Socks.Add(sock);
            db.SaveChanges();
        }

        private static double RandomNumberBetween(double minValue, double maxValue)
        {
            var next = random.NextDouble();
            return minValue + (next * (maxValue - minValue));
        }
        public static void Run() { }
    }

    public class Singleton
    {
        private static Singleton _instance = null;
        private static object chekLock = new object();
        private Singleton() { }

        public static Singleton Instance

        {
            get
            {
                lock (chekLock)
                {
                    if (_instance == null)
                        _instance = new Singleton();
                    return _instance;
                }
            }
        }

        public void StartPriceUpdating()
        {
            SockPriceUpdater.Run();
        }
    }
}