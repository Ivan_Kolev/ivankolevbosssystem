﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IvanKolevBossSystem.Startup))]
namespace IvanKolevBossSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
