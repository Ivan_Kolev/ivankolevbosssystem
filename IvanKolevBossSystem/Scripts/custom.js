﻿$(document).ready(function () {
    var button = $('#deposit');
    var serviceURL = '/Ajax/Deposit';

    var transactionData = {};
    
    button.click(function (event) {

        transactionData.userId = $('#id').val();
        transactionData.money = $('#money').val();
        transactionData.typeOfTransaction = $('#typeOfTransaction').val();
        transactionData = JSON.stringify(transactionData);

        console.log(transactionData)
        $.ajax({
            type: "POST",
            url: serviceURL,
            data: transactionData,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: successFunc,
            error: errorFunc
        });

        function successFunc(data) {
            //update user acc balance
            showInfo(data);
        }

        function errorFunc() {
            alert('error');
        }
    });
});

function showInfo(message) {
    $('#infoBox').text(message);
    $('#infoBox').show();
    setTimeout(function () {
        $('#infoBox').fadeOut();
    }, 3000);
}
function showError(errorMsg) {
    $('#errorBox').text("Error: " + errorMsg);
    $('#errorBox').show();
    setTimeout(function () {
        $('#infoBox').fadeOut();
    }, 3000);
}