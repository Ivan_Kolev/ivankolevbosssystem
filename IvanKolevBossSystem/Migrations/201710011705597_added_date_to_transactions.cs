namespace IvanKolevBossSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_date_to_transactions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "Date");
        }
    }
}
