namespace IvanKolevBossSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class entities_changed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "SockAmount", c => c.Int(nullable: false));
            AddColumn("dbo.Transactions", "CurrentSockPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Transactions", "CurrentAccBalance", c => c.String(nullable: false));
            AddColumn("dbo.AspNetUsers", "SocksCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "SocksCount");
            DropColumn("dbo.Transactions", "CurrentAccBalance");
            DropColumn("dbo.Transactions", "CurrentSockPrice");
            DropColumn("dbo.Transactions", "SockAmount");
        }
    }
}
